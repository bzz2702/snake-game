from turtle import Turtle


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.score = 0
        with open("high_score.txt") as file:
            high_score = file.read()
            self.high_score = int(high_score)
        self.show_score()

    def show_score(self):
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(0, 260)
        self.write(f"Score: {self.score} High Score: {self.high_score}", True, align="center", font=('Arial', 25, 'normal'))

    def score_up(self):
        self.clear()
        self.score += 1

    def reset(self):
        if self.score > self.high_score:
            self.high_score = self.score
            with open("high_score.txt", "w") as file:
                file.write(f"{self.high_score}")
        self.score = 0
        self.clear()
        self.show_score()

    # def game_over(self):
    #     self.goto(0, 0)
    #     self.write(f"GAME OVER", True, align="center", font=('Arial', 25, 'normal'))